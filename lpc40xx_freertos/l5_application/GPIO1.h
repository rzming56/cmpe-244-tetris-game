#include <stdbool.h>

#include "FreeRTOS.h"
#include "task.h"

#include "clock.h"
#include "delay.h"
#include "gpio.h"

#include "lpc40xx.h"
#include "uart.h"
#include "uart_printf.h"

typedef struct {
  uint8_t port;
  uint8_t pin;
  bool external;
} gpio_struct;

/// Should alter the hardware registers to set the pin as input
void gpio0__set_as_input(uint8_t pin_num, uint8_t port);

/// Should alter the hardware registers to set the pin as output
void gpio0__set_as_output(uint8_t pin_num, uint8_t port);

/// Should alter the hardware registers to set the pin as high
void gpio0__set_high(uint8_t pin_num, uint8_t port);

/// Should alter the hardware registers to set the pin as low
void gpio0__set_low(uint8_t pin_num, uint8_t port);

/**
 * Should alter the hardware registers to set the pin as low
 *
 * @param {bool} high - true => set pin high, false => set pin low
 */
void gpio0__set(uint8_t pin_num, uint8_t port, bool high);

/**
 * Should return the state of the pin (input or output, doesn't matter)
 *
 * @return {bool} level of pin high => true, low => false
 */
bool gpio0__get_level(uint8_t pin_num, uint8_t port);

void led_task(void *task_param);
void switch_task(void *task_param);
void led_extra(void *task_param);