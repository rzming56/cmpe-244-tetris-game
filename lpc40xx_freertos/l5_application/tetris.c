#include "tetris.h"
#include <stdbool.h>

int state_counter = 0;
static bool initial_piece_saved = false;
static uint8_t pushed_piece_id;
static uint8_t saved_piece_id;

piece_in_play init_play_piece(int piece_selector) {
  piece_in_play piece;
  state_counter = 0;
  if (piece_selector == 1) { // Blue Ricky ( L )
    piece.x1 = 4;
    piece.y1 = 22; // (4,22)
    piece.x2 = 5;
    piece.y2 = 22; // (5,22)  center for turn
    piece.x3 = 6;
    piece.y3 = 22; // (6,22)
    piece.x4 = 6;
    piece.y4 = 23; // (6,23)
    piece.px1 = 4;
    piece.py1 = 22;
    piece.px2 = 5;
    piece.py2 = 22;
    piece.px3 = 6;
    piece.py3 = 22;
    piece.px4 = 6;
    piece.py4 = 23;
    piece.color = 1; // Blue
    piece.complete = 0;
  } else if (piece_selector == 2) {
    piece.x1 = 4;
    piece.y1 = 23; // Cleaveland (Z)
    piece.x2 = 5;
    piece.y2 = 23;
    piece.x3 = 5;
    piece.y3 = 22; // center for turn
    piece.x4 = 6;
    piece.y4 = 22;
    piece.px1 = 4;
    piece.py1 = 23;
    piece.px2 = 5;
    piece.py2 = 23;
    piece.px3 = 5;
    piece.py3 = 22;
    piece.px4 = 6;
    piece.py4 = 22;
    piece.color = 2; // Green
    piece.complete = 0;
  } else if (piece_selector == 3) { // Hero (line)
    piece.x1 = 3;
    piece.y1 = 23;
    piece.x2 = 4;
    piece.y2 = 23; // center for left turn
    piece.x3 = 5;
    piece.y3 = 23; // center for right turn
    piece.x4 = 6;
    piece.y4 = 23;
    piece.px1 = 3;
    piece.py1 = 23;
    piece.px2 = 4;
    piece.py2 = 23;
    piece.px3 = 5;
    piece.py3 = 23;
    piece.px4 = 6;
    piece.py4 = 23;
    piece.color = 3; // Teal
    piece.complete = 0;
  } else if (piece_selector == 4) { // Rhode Island Z (backwards Z)
    piece.x1 = 4;
    piece.y1 = 22;
    piece.x2 = 5;
    piece.y2 = 22; // center for turn
    piece.x3 = 5;
    piece.y3 = 23;
    piece.x4 = 6;
    piece.y4 = 23;
    piece.px1 = 4;
    piece.py1 = 22;
    piece.px2 = 5;
    piece.py2 = 22;
    piece.px3 = 5;
    piece.py3 = 23;
    piece.px4 = 6;
    piece.py4 = 23;
    piece.color = 4; // Red
    piece.complete = 0;
  } else if (piece_selector == 5) { // T Teewee
    piece.x1 = 4;
    piece.y1 = 22;
    piece.x2 = 5;
    piece.y2 = 22; // center for turn
    piece.x3 = 6;
    piece.y3 = 22;
    piece.x4 = 5;
    piece.y4 = 23;
    piece.px1 = 4;
    piece.py1 = 22;
    piece.px2 = 5;
    piece.py2 = 22;
    piece.px3 = 6;
    piece.py3 = 22;
    piece.px4 = 5;
    piece.py4 = 23;
    piece.color = 5; // Purple
    piece.complete = 0;
  } else if (piece_selector == 6) { // Square
    piece.x1 = 4;
    piece.y1 = 22;
    piece.x2 = 5;
    piece.y2 = 22;
    piece.x3 = 4;
    piece.y3 = 23; // no turn
    piece.x4 = 5;
    piece.y4 = 23;
    piece.px1 = 4;
    piece.py1 = 22;
    piece.px2 = 5;
    piece.py2 = 22;
    piece.px3 = 4;
    piece.py3 = 23;
    piece.px4 = 5;
    piece.py4 = 23;
    piece.color = 6; // Yellow
    piece.complete = 0;
  } else if (piece_selector == 7) { // backwards L
    piece.x1 = 4;
    piece.y1 = 23;
    piece.x2 = 4;
    piece.y2 = 22;
    piece.x3 = 5;
    piece.y3 = 22; // center for right turn
    piece.x4 = 6;
    piece.y4 = 22;
    piece.px1 = 4;
    piece.py1 = 23;
    piece.px2 = 4;
    piece.py2 = 22;
    piece.px3 = 5;
    piece.py3 = 22;
    piece.px4 = 6;
    piece.py4 = 22;
    piece.color = 7; // white
    piece.complete = 0;
  }

  return piece;
}

uint8_t game_place_piece(piece_in_play *piece) {

  uint8_t r = 0;
  uint8_t g = 0;
  uint8_t b = 0;

  if (piece->color == 1) {
    b = 1;
  } else if (piece->color == 2) {
    g = 1;
  } else if (piece->color == 3) {
    g = 1;
    b = 1;
  } else if (piece->color == 4) {
    r = 1;
  } else if (piece->color == 5) {
    r = 1;
    b = 1;
  } else if (piece->color == 6) {
    r = 1;
    g = 1;
  } else if (piece->color == 7) {
    r = 1;
    g = 1;
    b = 1;
  }

  if (piece->complete == 0) {
    if (board_check_piece(piece)) {
      game_draw_piece(piece, r, g, b, 1);
      piece->complete = 1;
      game_draw_game_board(0);
      return board_check_end_game(piece);
    }

    // keep track of previous location -> clear and move
    game_draw_piece(piece, 0, 0, 0, 1);
    game_draw_piece(piece, r, g, b, 0);

    piece->px1 = piece->x1;
    piece->px2 = piece->x2;
    piece->px3 = piece->x3;
    piece->px4 = piece->x4;
    piece->py1 = piece->y1;
    piece->py2 = piece->y2;
    piece->py3 = piece->y3;
    piece->py4 = piece->y4;

    piece->y1 -= 1;
    piece->y2 -= 1;
    piece->y3 -= 1;
    piece->y4 -= 1;
  }
  game_draw_game_board(0);
  return 1;
}

void game_draw_piece(piece_in_play *piece, uint8_t r, uint8_t g, uint8_t b, uint8_t prev) {
  if (prev == 1) {
    board_draw_single_pixel(piece->px1, piece->py1, r, g, b);
    board_draw_single_pixel(piece->px2, piece->py2, r, g, b);
    board_draw_single_pixel(piece->px3, piece->py3, r, g, b);
    board_draw_single_pixel(piece->px4, piece->py4, r, g, b);
  } else {
    board_draw_single_pixel(piece->x1, piece->y1, r, g, b);
    board_draw_single_pixel(piece->x2, piece->y2, r, g, b);
    board_draw_single_pixel(piece->x3, piece->y3, r, g, b);
    board_draw_single_pixel(piece->x4, piece->y4, r, g, b);
  }
}

void board_clear_game_board(uint8_t index) {

  if (index != 23) {
    game_board_green[index + 2] = 0x3FF;
    game_board_blue[index + 2] = 0x3FF;
  }
  game_board_green[index + 1] = 0x3FF;
  game_board_blue[index + 1] = 0x3FF;
  game_board_red[index] = 0;
  game_board_green[index] = 0;
  game_board_blue[index] = 0;
  game_draw_game_board(0);
}

uint8_t board_check_end_game(piece_in_play *piece) {

  if (piece->py1 >= 23 || piece->py2 >= 23 || piece->py3 >= 23 || piece->py4 >= 23) {
    // out bounds
    return 0;
  }
  return 1;
}

uint8_t board_check_occupied(uint8_t column, uint8_t row) {
  if (game_board_blue[row] & (1 << column) || row > 25)
    return 1;
  if (game_board_red[row] & (1 << column) || row > 25)
    return 1;
  if (game_board_green[row] & (1 << column) || row > 25)
    return 1;

  return 0;
}

uint8_t board_check_previous_location(piece_in_play *piece, uint8_t x, uint8_t y) {
  if ((x == piece->px1 && y == piece->py1) || (x == piece->px2 && y == piece->py2) ||
      (x == piece->px3 && y == piece->py3) || (x == piece->px4 && y == piece->py4))
    return 1; // this point is your past location or one of your own piece points
  return 0;
}

uint8_t board_check_piece(piece_in_play *piece) {
  uint8_t occupied = 0;
  if (board_check_occupied(piece->x1, piece->y1)) {
    if (board_check_previous_location(piece, piece->x1, piece->y1))
      ; // false positive
    else
      occupied = 1;
  }

  if (board_check_occupied(piece->x2, piece->y2)) {
    if (board_check_previous_location(piece, piece->x2, piece->y2))
      ; // false positive
    else
      occupied = 1;
  }

  if (board_check_occupied(piece->x3, piece->y3)) {
    if (board_check_previous_location(piece, piece->x3, piece->y3))
      ; // false positive
    else
      occupied = 1;
  }

  if (board_check_occupied(piece->x4, piece->y4)) {
    if (board_check_previous_location(piece, piece->x4, piece->y4))
      ; // false positive
    else
      occupied = 1;
  }
  return occupied; // No collisions
}

uint8_t game_check_full_lines(void) {

  uint8_t returned_score = 0;
  uint8_t tetris_line_count = 0;

  for (int i = 0; i < 24; i++) {
    // Check the lines to see if it is full by ORing the lines. Since pieces
    // can be of any color
    if ((game_board_blue[i] | game_board_red[i] | game_board_green[i]) == 0x3FF) {

      tetris_line_count++;
      for (int a = i; a < 23; a++) {
        // Shift every row down
        game_board_green[a] = game_board_green[a + 1];
        game_board_blue[a] = game_board_blue[a + 1];
        game_board_red[a] = game_board_red[a + 1];
      }
      game_board_green[23] = 0;
      game_board_red[23] = 0;
      game_board_blue[23] = 0;
      i--;
    } else {
      returned_score += tetris_line_count;
      tetris_line_count = 0;
    }
    if (tetris_line_count == 4) {
      returned_score += 8;
      tetris_line_count = 0;
    }

    game_draw_game_board(0);
  }
  return returned_score;
}

void tetris__rotate(piece_in_play *piece) {
  state_counter++;
  if (state_counter % 4 == 0) {
    state_counter = 0;
  }
  // each piece will have 4 rotation states.
  switch (piece->color) {
  case blue_ricky: // Blue Ricky ( L )
  {
    if (piece->x2 == 0 || piece->x2 == 9) {
      ;
    } else {
      if (state_counter == 0) // Original Position
      {
        piece->x1 = piece->x2 - 1;
        piece->y1 = piece->y2;
        piece->x2 = piece->x2;
        piece->y2 = piece->y2; // center
        piece->x3 = piece->x2 + 1;
        piece->y3 = piece->y2;
        piece->x4 = piece->x2 + 1;
        piece->y4 = piece->y2 + 1;
        if (board_check_piece(piece)) {
          piece->x1 = piece->x2;
          piece->y1 = piece->y2 - 1;
          piece->x2 = piece->x2;
          piece->y2 = piece->y2; // center
          piece->x3 = piece->x2;
          piece->y3 = piece->y2 + 1;
          piece->x4 = piece->x2 - 1;
          piece->y4 = piece->y2 + 1;
          state_counter = 3;
        }
      } else if (state_counter == 1) {
        piece->x1 = piece->x2;
        piece->y1 = piece->y2 + 1;
        piece->x2 = piece->x2;
        piece->y2 = piece->y2; // center
        piece->x3 = piece->x2;
        piece->y3 = piece->y2 - 1;
        piece->x4 = piece->x2 + 1;
        piece->y4 = piece->y2 - 1;
        if (board_check_piece(piece)) {
          piece->x1 = piece->x2 - 1;
          piece->y1 = piece->y2;
          piece->x2 = piece->x2;
          piece->y2 = piece->y2; // center
          piece->x3 = piece->x2 + 1;
          piece->y3 = piece->y2;
          piece->x4 = piece->x2 + 1;
          piece->y4 = piece->y2 + 1;
          state_counter--;
        }
      } else if (state_counter == 2) {
        piece->x1 = piece->x2 + 1;
        piece->y1 = piece->y2;
        piece->x2 = piece->x2;
        piece->y2 = piece->y2; // center
        piece->x3 = piece->x2 - 1;
        piece->y3 = piece->y2;
        piece->x4 = piece->x2 - 1;
        piece->y4 = piece->y2 - 1;
        if (board_check_piece(piece)) {
          piece->x1 = piece->x2;
          piece->y1 = piece->y2 + 1;
          piece->x2 = piece->x2;
          piece->y2 = piece->y2; // center
          piece->x3 = piece->x2;
          piece->y3 = piece->y2 - 1;
          piece->x4 = piece->x2 + 1;
          piece->y4 = piece->y2 - 1;
          state_counter--;
        }
      } else if (state_counter == 3) {
        piece->x1 = piece->x2;
        piece->y1 = piece->y2 - 1;
        piece->x2 = piece->x2;
        piece->y2 = piece->y2; // center
        piece->x3 = piece->x2;
        piece->y3 = piece->y2 + 1;
        piece->x4 = piece->x2 - 1;
        piece->y4 = piece->y2 + 1;
        if (board_check_piece(piece)) {
          piece->x1 = piece->x2 + 1;
          piece->y1 = piece->y2;
          piece->x2 = piece->x2;
          piece->y2 = piece->y2; // center
          piece->x3 = piece->x2 - 1;
          piece->y3 = piece->y2;
          piece->x4 = piece->x2 - 1;
          piece->y4 = piece->y2 - 1;
          state_counter--;
        }
      }
    }
    break;
  }
  case cleaveland_z: {
    if (piece->x3 == 0) {
      ;
    } else if (state_counter == 0 || state_counter == 2) // Original Position
    {
      piece->x1 = piece->x3 - 1;
      piece->y1 = piece->y3 + 1;
      piece->x2 = piece->x3;
      piece->y2 = piece->y3 + 1;
      piece->x3 = piece->x3; // center
      piece->y3 = piece->y3;
      piece->x4 = piece->x3 + 1;
      piece->y4 = piece->y3;
      if (board_check_piece(piece)) {
        piece->x1 = piece->x3 + 1;
        piece->y1 = piece->y3 + 1;
        piece->x2 = piece->x3 + 1;
        piece->y2 = piece->y3;
        piece->x3 = piece->x3;
        piece->y3 = piece->y3; // center
        piece->x4 = piece->x3;
        piece->y4 = piece->y3 - 1;
        if (state_counter == 0) {
          state_counter = 3;
        } else {
          state_counter--;
        }
      }
    } else if (state_counter == 1 || state_counter == 3) {
      piece->x1 = piece->x3 + 1;
      piece->y1 = piece->y3 + 1;
      piece->x2 = piece->x3 + 1;
      piece->y2 = piece->y3;
      piece->x3 = piece->x3;
      piece->y3 = piece->y3; // center
      piece->x4 = piece->x3;
      piece->y4 = piece->y3 - 1;
      if (board_check_piece(piece)) {
        piece->x1 = piece->x3 - 1;
        piece->y1 = piece->y3 + 1;
        piece->x2 = piece->x3;
        piece->y2 = piece->y3 + 1;
        piece->x3 = piece->x3; // center
        piece->y3 = piece->y3;
        piece->x4 = piece->x3 + 1;
        piece->y4 = piece->y3;
        state_counter--;
      }
    }
    break;
  }
  case hero: // Hero Line ( | )
  {
    if ((piece->x3 == 0 && piece->x2 == 0) || (piece->x3 == 1 && piece->x2 == 1) ||
        (piece->x3 == 9 && piece->x2 == 9)) {
      ;
    } else if (state_counter == 0 || state_counter == 2) // Original Position
    {
      piece->x1 = piece->x3 - 2;
      piece->y1 = piece->y3;
      piece->x2 = piece->x3 - 1;
      piece->y2 = piece->y3;
      piece->x3 = piece->x3;
      piece->y3 = piece->y3; // center for right turn
      piece->x4 = piece->x3 + 1;
      piece->y4 = piece->y3;
      if (board_check_piece(piece)) {
        piece->x1 = piece->x3;
        piece->y1 = piece->y3 + 2;
        piece->x2 = piece->x3;
        piece->y2 = piece->y3 + 1;
        piece->x3 = piece->x3;
        piece->y3 = piece->y3; // center for right turn
        piece->x4 = piece->x3;
        piece->y4 = piece->y3 - 1;
        if (state_counter == 0) {
          state_counter = 3;
        } else {
          state_counter--;
        }
      }
    } else if (state_counter == 1 || state_counter == 3) {
      piece->x1 = piece->x3;
      piece->y1 = piece->y3 + 2;
      piece->x2 = piece->x3;
      piece->y2 = piece->y3 + 1;
      piece->x3 = piece->x3;
      piece->y3 = piece->y3; // center for right turn
      piece->x4 = piece->x3;
      piece->y4 = piece->y3 - 1;
      if (board_check_piece(piece)) {
        piece->x1 = piece->x3 - 2;
        piece->y1 = piece->y3;
        piece->x2 = piece->x3 - 1;
        piece->y2 = piece->y3;
        piece->x3 = piece->x3;
        piece->y3 = piece->y3; // center for right turn
        piece->x4 = piece->x3 + 1;
        piece->y4 = piece->y3;
        state_counter--;
      }
    }
    break;
  }
  case rhode_z: // Rhode Island Z (backwards Z)
  {
    if (piece->x2 == 0) {
      ;
    } else if (state_counter == 0 || state_counter == 2) // Original Position
    {
      piece->x1 = piece->x2 - 1;
      piece->y1 = piece->y2;
      piece->x2 = piece->x2;
      piece->y2 = piece->y2; // center
      piece->x3 = piece->x2;
      piece->y3 = piece->y2 + 1;
      piece->x4 = piece->x2 + 1;
      piece->y4 = piece->y2 + 1;
      if (board_check_piece(piece)) {
        piece->x1 = piece->x2;
        piece->y1 = piece->y2 + 1;
        piece->x2 = piece->x2;
        piece->y2 = piece->y2; // center
        piece->x3 = piece->x2 + 1;
        piece->y3 = piece->y2;
        piece->x4 = piece->x2 + 1;
        piece->y4 = piece->y2 - 1;
        if (state_counter == 0) {
          state_counter = 3;
        } else {
          state_counter--;
        }
      }
    } else if (state_counter == 1 || state_counter == 3) {
      piece->x1 = piece->x2;
      piece->y1 = piece->y2 + 1;
      piece->x2 = piece->x2;
      piece->y2 = piece->y2; // center
      piece->x3 = piece->x2 + 1;
      piece->y3 = piece->y2;
      piece->x4 = piece->x2 + 1;
      piece->y4 = piece->y2 - 1;
      if (board_check_piece(piece)) {
        piece->x1 = piece->x2 - 1;
        piece->y1 = piece->y2;
        piece->x2 = piece->x2;
        piece->y2 = piece->y2; // center
        piece->x3 = piece->x2;
        piece->y3 = piece->y2 + 1;
        piece->x4 = piece->x2 + 1;
        piece->y4 = piece->y2 + 1;
        state_counter--;
      }
    }
    break;
  }
  case teewee: // Teewee ( T )
  {
    if (piece->x2 == 0 || piece->x2 == 9) {
      ;
    } else if (state_counter == 0) // Original Position
    {
      piece->x1 = piece->x2 - 1;
      piece->y1 = piece->y2;
      piece->x2 = piece->x2;
      piece->y2 = piece->y2; // center
      piece->x3 = piece->x2 + 1;
      piece->y3 = piece->y2;
      piece->x4 = piece->x2;
      piece->y4 = piece->y2 + 1;
      if (board_check_piece(piece)) {
        piece->x1 = piece->x2;
        piece->y1 = piece->y2 - 1;
        piece->x2 = piece->x2;
        piece->y2 = piece->y2; // center
        piece->x3 = piece->x2;
        piece->y3 = piece->y2 + 1;
        piece->x4 = piece->x2 - 1;
        piece->y4 = piece->y2;
        state_counter = 3;
      }
    } else if (state_counter == 1) {
      piece->x1 = piece->x2;
      piece->y1 = piece->y2 + 1;
      piece->x2 = piece->x2;
      piece->y2 = piece->y2; // center for turn
      piece->x3 = piece->x2;
      piece->y3 = piece->y2 - 1;
      piece->x4 = piece->x2 + 1;
      piece->y4 = piece->y2;
      if (board_check_piece(piece)) {
        piece->x1 = piece->x2 + 1;
        piece->y1 = piece->y2;
        piece->x2 = piece->x2;
        piece->y2 = piece->y2; // center
        piece->x3 = piece->x2 - 1;
        piece->y3 = piece->y2;
        piece->x4 = piece->x2;
        piece->y4 = piece->y2 - 1;
        state_counter--;
      }
    } else if (state_counter == 2) {
      piece->x1 = piece->x2 + 1;
      piece->y1 = piece->y2;
      piece->x2 = piece->x2;
      piece->y2 = piece->y2; // center
      piece->x3 = piece->x2 - 1;
      piece->y3 = piece->y2;
      piece->x4 = piece->x2;
      piece->y4 = piece->y2 - 1;
      if (board_check_piece(piece)) {
        piece->x1 = piece->x2;
        piece->y1 = piece->y2 + 1;
        piece->x2 = piece->x2;
        piece->y2 = piece->y2; // center
        piece->x3 = piece->x2;
        piece->y3 = piece->y2 - 1;
        piece->x4 = piece->x2 + 1;
        piece->y4 = piece->y2;
        state_counter--;
      }
    } else if (state_counter == 3) {
      piece->x1 = piece->x2;
      piece->y1 = piece->y2 - 1;
      piece->x2 = piece->x2;
      piece->y2 = piece->y2; // center
      piece->x3 = piece->x2;
      piece->y3 = piece->y2 + 1;
      piece->x4 = piece->x2 - 1;
      piece->y4 = piece->y2;
      if (board_check_piece(piece)) {
        piece->x1 = piece->x2 + 1;
        piece->y1 = piece->y2;
        piece->x2 = piece->x2;
        piece->y2 = piece->y2; // center
        piece->x3 = piece->x2 - 1;
        piece->y3 = piece->y2;
        piece->x4 = piece->x2;
        piece->y4 = piece->y2 - 1;
        state_counter--;
      }
    }
    break;
  }
  case smashboy: // Square ( |_| )
  {
    // no need to rotate
    break;
  }
  case white_ricky: // backwards L
  {
    if (piece->x3 == 0 || piece->x3 == 9) {
      ;
    } else if (state_counter == 0) // Original Position
    {
      piece->x1 = piece->x3 - 1;
      piece->y1 = piece->y3 + 1;
      piece->x2 = piece->x3 - 1;
      piece->y2 = piece->y3;
      piece->x3 = piece->x3;
      piece->y3 = piece->y3; // center
      piece->x4 = piece->x3 + 1;
      piece->y4 = piece->y3;
      if (board_check_piece(piece)) {
        piece->x1 = piece->x3 - 1;
        piece->y1 = piece->y3 - 1;
        piece->x2 = piece->x3;
        piece->y2 = piece->y3 - 1;
        piece->x3 = piece->x3;
        piece->y3 = piece->y3; // center
        piece->x4 = piece->x3;
        piece->y4 = piece->y3 + 1;
        state_counter = 3;
      }
    } else if (state_counter == 1) {
      piece->x1 = piece->x3 + 1;
      piece->y1 = piece->y3 + 1;
      piece->x2 = piece->x3;
      piece->y2 = piece->y3 + 1;
      piece->x3 = piece->x3;
      piece->y3 = piece->y3; // center
      piece->x4 = piece->x3;
      piece->y4 = piece->y3 - 1;
      if (board_check_piece(piece)) {
        piece->x1 = piece->x3 - 1;
        piece->y1 = piece->y3 + 1;
        piece->x2 = piece->x3 - 1;
        piece->y2 = piece->y3;
        piece->x3 = piece->x3;
        piece->y3 = piece->y3; // center
        piece->x4 = piece->x3 + 1;
        piece->y4 = piece->y3;
        state_counter--;
      }
    } else if (state_counter == 2) {
      piece->x1 = piece->x3 + 1;
      piece->y1 = piece->y3 - 1;
      piece->x2 = piece->x3 + 1;
      piece->y2 = piece->y3;
      piece->x3 = piece->x3;
      piece->y3 = piece->y3; // center
      piece->x4 = piece->x3 - 1;
      piece->y4 = piece->y3;
      if (board_check_piece(piece)) {
        piece->x1 = piece->x3 + 1;
        piece->y1 = piece->y3 + 1;
        piece->x2 = piece->x3;
        piece->y2 = piece->y3 + 1;
        piece->x3 = piece->x3;
        piece->y3 = piece->y3; // center
        piece->x4 = piece->x3;
        piece->y4 = piece->y3 - 1;
        state_counter--;
      }
    } else if (state_counter == 3) {
      piece->x1 = piece->x3 - 1;
      piece->y1 = piece->y3 - 1;
      piece->x2 = piece->x3;
      piece->y2 = piece->y3 - 1;
      piece->x3 = piece->x3;
      piece->y3 = piece->y3; // center
      piece->x4 = piece->x3;
      piece->y4 = piece->y3 + 1;
      if (board_check_piece(piece)) {
        piece->x1 = piece->x3 + 1;
        piece->y1 = piece->y3 - 1;
        piece->x2 = piece->x3 + 1;
        piece->y2 = piece->y3;
        piece->x3 = piece->x3;
        piece->y3 = piece->y3; // center
        piece->x4 = piece->x3 - 1;
        piece->y4 = piece->y3;
        state_counter--;
      }
    }
    break;
  }
  default:
    break;
  }
}
void tetris__player_move_right(piece_in_play *piece) {
  if (piece->x1 == 0 || piece->x2 == 0 || piece->x3 == 0 || piece->x4 == 0) {
    ;
  } else {
    piece->x1 = piece->x1 - 1;
    piece->x2 = piece->x2 - 1;
    piece->x3 = piece->x3 - 1;
    piece->x4 = piece->x4 - 1;
    if (board_check_piece(piece)) {
      piece->x1 = piece->x1 + 1;
      piece->x2 = piece->x2 + 1;
      piece->x3 = piece->x3 + 1;
      piece->x4 = piece->x4 + 1;
    }
  }
}

void tetris__player_move_left(piece_in_play *piece) {
  if (piece->x1 == 9 || piece->x2 == 9 || piece->x3 == 9 || piece->x4 == 9) {
    ;
  } else {
    piece->x1 = piece->x1 + 1;
    piece->x2 = piece->x2 + 1;
    piece->x3 = piece->x3 + 1;
    piece->x4 = piece->x4 + 1;
    if (board_check_piece(piece)) {
      piece->x1 = piece->x1 - 1;
      piece->x2 = piece->x2 - 1;
      piece->x3 = piece->x3 - 1;
      piece->x4 = piece->x4 - 1;
    }
  }
}

uint8_t tetris__save_piece(piece_in_play *piece) {

  game_board_save_piece(piece->color);
  if (initial_piece_saved == false) // When saving for the first time.
  {
    saved_piece_id = piece->color;
    initial_piece_saved = true;
  } else // When swapping current piece in play with saved/held piece.
  {
    pushed_piece_id = saved_piece_id;
    saved_piece_id = piece->color;
    // printf("saved piece id:%i\n", saved_piece_id);
    return pushed_piece_id;
  }
}