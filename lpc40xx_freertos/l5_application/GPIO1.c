
#include "GPIO1.h"

bool led_active = false;
bool led_active_ext = false;

void gpio0__set_as_input(uint8_t pin_num, uint8_t port) {
  if (port == 0)
    LPC_GPIO0->DIR &= ~(1 << pin_num);
  else if (port == 1)
    LPC_GPIO1->DIR &= ~(1 << pin_num);
  else if (port == 2)
    LPC_GPIO2->DIR &= ~(1 << pin_num);
}

/// Should alter the hardware registers to set the pin as output
void gpio0__set_as_output(uint8_t pin_num, uint8_t port) {
  if (port == 0)
    LPC_GPIO0->DIR |= (1 << pin_num);
  else if (port == 1)
    LPC_GPIO1->DIR |= (1 << pin_num);
  else if (port == 2)
    LPC_GPIO2->DIR |= (1 << pin_num);
}
/// Should alter the hardware registers to set the pin as high
void gpio0__set_high(uint8_t pin_num, uint8_t port) {
  if (port == 0)
    LPC_GPIO0->SET |= (1 << pin_num);
  else if (port == 1)
    LPC_GPIO1->SET |= (1 << pin_num);
  else if (port == 2)
    LPC_GPIO2->SET |= (1 << pin_num);
}
/// Should alter the hardware registers to set the pin as low
void gpio0__set_low(uint8_t pin_num, uint8_t port) {
  if (port == 0)
    LPC_GPIO0->CLR |= (1 << pin_num);
  else if (port == 1)
    LPC_GPIO1->CLR |= (1 << pin_num);
  else if (port == 2)
    LPC_GPIO2->CLR |= (1 << pin_num);
}

/**
 * Should alter the hardware registers to set the pin as low
 *
 * @param {bool} high - true => set pin high, false => set pin low
 */
void gpio0__set(uint8_t pin_num, uint8_t port, bool high) {
  if (port == 0) {
    if (high)
      LPC_GPIO0->SET |= (1 << pin_num);
    else
      LPC_GPIO0->CLR |= (1 << pin_num);
  } else if (port == 1) {
    if (high)
      LPC_GPIO1->SET |= (1 << pin_num);
    else
      LPC_GPIO1->CLR |= (1 << pin_num);
  } else if (port == 2) {
    if (high)
      LPC_GPIO2->SET |= (1 << pin_num);
    else
      LPC_GPIO2->CLR |= (1 << pin_num);
  }
}

/**
 * Should return the state of the pin (input or output, doesn't matter)
 *
 * @return {bool} level of pin high => true, low => false
 */
bool gpio0__get_level(uint8_t pin_num, uint8_t port) {
  if (port == 0) {
    if (LPC_GPIO0->PIN & (1 << pin_num))
      return true;
    else
      return false;
  } else if (port == 1) {
    if (LPC_GPIO1->PIN & (1 << pin_num))
      return true;
    else
      return false;
  } else if (port == 2) {
    if (LPC_GPIO2->PIN & (1 << pin_num))
      return true;
    else
      return false;
  }
  return false;
}

/*
PART 0:

void led_task(void *pvParameters) {
    //Choose one of the onboard LEDS by looking into schematics and write code for the below

    LPC_GPIO2->DIR |= (1<<3);
    LPC_GPIO2->PIN |= (1<<3);

    while(1) {
      LPC_GPIO2->CLR |= (1<<3);
      vTaskDelay(500);
      LPC_GPIO2->SET |= (1<<3);
      vTaskDelay(500);
    }
}
*/
// Part 1 A/B
void led_task(void *task_param) {
  /* Get Parameter */
  gpio_struct *ptr = (gpio_struct *)task_param;
  uint8_t pin = ptr->pin;
  uint8_t port = ptr->port;
  bool external = ptr->external;
  /* Define Constants Here */

  /* Define Local Variables and Objects */
  /* Initialization Code */

  gpio0__set_as_output(pin, port);
  while (1) {
    if (external == false) {
      if (led_active == true) {
        if (gpio0__get_level(pin, port))
          gpio0__set(pin, port, false);
        else
          gpio0__set(pin, port, true);

        vTaskDelay(200);
        led_active = false;
      }
    } else {
      if (led_active_ext == true) {
        if (gpio0__get_level(pin, port))
          gpio0__set(pin, port, false);
        else
          gpio0__set(pin, port, true);

        vTaskDelay(200);
        led_active_ext = false;
      }
    }
    vTaskDelay(10);
  }
}
/*
void led_task_ext(void *task_param)
{
    gpio_struct* ptr = (gpio_struct*)task_param;
    uint8_t pin = ptr->pin;
    uint8_t port = ptr->port;

    gpio0__set_as_output(pin,port);
    while(1) {

      if(led_active_ext == true)
      {
        if(gpio0__get_level(pin,port))
          gpio0__set(pin,port,false);
        else
          gpio0__set(pin,port,true);

        vTaskDelay(200);
        led_active_ext = false;
      }
      vTaskDelay(10);
    }
}
*/
void switch_task(void *task_param) {
  /* Get Parameter */
  gpio_struct *ptr = (gpio_struct *)task_param;
  uint8_t pin = ptr->pin;
  uint8_t port = ptr->port;
  bool external = ptr->external;
  /* Define Constants Here */

  /* Define Local Variables and Objects */
  bool pressed = false;
  /* Initialization Code */
  gpio0__set_as_input(pin, port);

  while (1) {
    if (external == false) {
      if (gpio0__get_level(pin, port)) {
        pressed = true;
        led_active = false;
      } else {
        if (pressed == true) {
          led_active = true;
          pressed = false;
          vTaskDelay(200);
        }
      }
    } else {
      if (gpio0__get_level(pin, port)) {
        pressed = true;
        led_active_ext = false;
      } else {
        if (pressed == true) {
          led_active_ext = true;
          pressed = false;
          vTaskDelay(200);
        }
      }
    }
  }
}
/*
void switch_task_ext(void * task_param)
{

    uint32_t param = (uint32_t)(task_param);

    bool pressed = false;

    gpio0__set_as_input(param,0);

    while(1) {
      if(gpio0__get_level(param,0))
      {
        pressed = true;
        led_active_ext = false;
      }
      else
      {
        if(pressed == true)
        {
          led_active_ext = true;
          pressed = false;
          vTaskDelay(200);
        }
        
      }
    }
}
*/
void led_extra(void *task_param) {
  gpio0__set_as_output(18, 1);
  gpio0__set_as_output(24, 1);
  gpio0__set_as_output(26, 1);
  gpio0__set_as_output(3, 2);
  gpio0__set_as_input(29, 0);
  gpio0__set_as_input(30, 0);

  while (1) {
    if (gpio0__get_level(30, 0)) {
      gpio0__set_low(18, 1);
      vTaskDelay(50);
      gpio0__set_low(24, 1);
      vTaskDelay(50);
      gpio0__set_low(26, 1);
      vTaskDelay(50);
      gpio0__set_low(3, 2);
      vTaskDelay(50);
      gpio0__set_high(18, 1);
      vTaskDelay(50);
      gpio0__set_high(24, 1);
      vTaskDelay(50);
      gpio0__set_high(26, 1);
      vTaskDelay(50);
      gpio0__set_high(3, 2);
      vTaskDelay(50);
    }
    vTaskDelay(100);
  }
}
