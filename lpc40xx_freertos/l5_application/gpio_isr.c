// @file gpio_isr.c
#include "gpio_isr.h"

// Note: You may want another separate array for falling vs. rising edge callbacks
static function_pointer_t gpio0_callbacks[32];

void gpio0__attach_interrupt(uint32_t pin, gpio_interrupt_e interrupt_type, function_pointer_t callback) {
  // 1) Store the callback based on the pin at gpio0_callbacks
  // 2) Configure GPIO 0 pin for rising or falling edge
  gpio0_callbacks[pin] = callback;

  if (interrupt_type == GPIO_INTR__FALLING_EDGE) {
    LPC_GPIOINT->IO0IntEnF |= (1 << pin);
  } else {
    LPC_GPIOINT->IO0IntEnR |= (1 << pin);
  }
}

// We wrote some of the implementation for you
void gpio0__interrupt_dispatcher(void) {
  // Check which pin generated the interrupt
  // const int pin_that_generated_interrupt = logic_that_you_will_write();
  const uint32_t interrupt_pin = gpio0_check_interrupt_status();
  uart_printf__polled(UART__0, "pin29_isr activated!\n");
  function_pointer_t attached_user_handler = gpio0_callbacks[interrupt_pin];

  // Invoke the user registered callback, and then clear the interrupt
  attached_user_handler();
  clear_pin_interrupt(interrupt_pin);
}

void clear_pin_interrupt(uint32_t pin) { LPC_GPIOINT->IO0IntClr |= (1 << pin); }

uint32_t gpio0_check_interrupt_status(void) {
  uint32_t bit_comparator = 0x01;
  for (int i = 0; i < 31; i++) {
    if (((LPC_GPIOINT->IO0IntStatR & bit_comparator) >> i) == 1) {
      uart_printf__polled(UART__0, "pin29_isr activated!\n");
      return i;
    }
    bit_comparator = bit_comparator << 1;
  }
  return 0;
}
